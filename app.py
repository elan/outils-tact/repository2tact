import re
import sys
import os
import requests
import zipfile
import glob
from slugify import slugify
import shutil


def createFile(slug, i, identifier):
    i = format(int(i) + 1, '04d')
    file = open(slug + '/file' + str(i) + '.xml', 'a')
    file.write("<identifier>" + identifier + "</identifier>")
    file.close()

    return i

def main():
    url = sys.argv[1]

    if "gallica" in url:
        suffix = "native"
        serverName = "Gallica"
        urlPrefix = "https://gallica.bnf.fr/iiif/"
        collectionIdentifier = re.findall('(ark:.*)', url)[0]
        manifest = requests.get(urlPrefix + collectionIdentifier + "/manifest.json").json()
        title = manifest["description"]
        slug = slugify(title, max_length=40)
        os.mkdir(slug)

        ressources = manifest["sequences"][0]["canvases"]

        i = 0
        for ressource in ressources:
            uri = ressource["@id"]
            id = re.findall('.*/(.*)', uri)[0]
            identifier = collectionIdentifier + "/" + id
            i = createFile(slug, i, identifier)

    elif "nakala" in url:
        suffix = "default"
        serverName = "Nakala"
        urlPrefix = "https://api.nakala.fr/iiif/"
        collectionId = re.findall('https://nakala.fr/(.*)', url)[0]
        manifest = requests.get("https://api.nakala.fr/datas/" + collectionId).json()
        title = manifest["citation"]
        slug = slugify(title, max_length=40)
        os.mkdir(slug)
        
        ressources = manifest["files"]
        
        i = 0
        for ressource in ressources:
            resourceId = ressource["sha1"]
            identifier = collectionId + "/" + resourceId
            i = createFile(slug, i, identifier)

    else:
        return 

    slugZip = "out/" + slug + '.zip'
    with zipfile.ZipFile(slugZip, 'w') as f:
        for file in glob.glob(slug + '/*'):
            f.write(file)

    shutil.rmtree(slug, ignore_errors=True)
    print("\n")
    print("------------------------------------------")
    print("-------------------- repository2tact -----")
    print("------------------------------------------")
    print("\n")
    print("Titre: " + title)
    print("URL: " + url)
    print("Images: " + str(len(ressources)))

    print("\n")
    print("Archive créée à envoyer dans TACT")
    print ("> Administrer > Média > Ajouter des médias à partir de sources externes")
    print("------------------------------------------")
    print(slugZip)
    
    print("\n")
    print("Paramètres à rentrer dans TACT")
    print("> Administrer > Gestion des serveurs d'images")
    print("------------------------------------------")
    print("Nom du serveur: " + serverName)
    print("URL du serveur: " + urlPrefix)
    print("suffixe large: " + "/full/,2000/0/" + suffix + ".jpg")
    print("suffixe miniature: " + "/full/,200/0/" + suffix + ".jpg")
    print("\n\n")

if __name__ == "__main__":
    main()
