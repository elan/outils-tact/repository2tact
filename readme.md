# repository2tact

Script python, qui prend en entrée l'url d'une ressource nakala/gallica et qui génère une archive qu'on peut charger dans TACT

## Installation
pip install -r requirements.txt

## Utilisation
```bash
python3 app.py <url>
```
par exemple
```bash
python3 app.py https://gallica.bnf.fr/ark:/12148/bpt6k87270839
```
ou 
```bash
python3 app.py https://nakala.fr/10.34847/nkl.deb655as
```

## Licence
GNU GENERAL PUBLIC LICENSE V3